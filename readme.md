# Addon Comix

## Developper quick start

### Launching storage services

```bash
# Starting Database service
docker run -d -p 27017:27017 -v ${PWD}/local-database:/data/db -e MONGO_INITDB_ROOT_USERNAME=devUser -e MONGO_INITDB_ROOT_PASSWORD=DevPasswordToChangeInProoood mongo
# Starting S3 buckets service
docker run -d -p 9000:9000 -p 9001:9001 -v ${PWD}/local-files:/data -e MINIO_ROOT_USER=devUser  -e MINIO_ROOT_PASSWORD=DevPasswordToChangeInProoood quay.io/minio/minio server /data --console-address ":9001"
```

### Configuring database (first setup only)

First, we want to ensure project unique name for a given author. To do this, open a Mongo Shell bound to the database previously created:

```
use comixAddon
db.projects.createIndex({"name": 1, "author_name": 1}, {unique: true});
```


### Configuring bucket (first setup only)

1. Hit http://127.0.0.1:9001 and login with the following user:pass : `devUser:DevPasswordToChangeInProoood`
2. Create a bucket named `comix`
3. Create an access key named `write-key` and store its value in the matching key of [backend/.env](backend/.env)
3. Create an access key named `read-key` and store its value in the matching key of [backend/.env](backend/.env)
4. **TODO: add policies instructions**-

### Launching app back and front

```bash
# Starting API service
cd backend
pipenv install
pipenv run uvicorn main:app --reload
```


Dans un autre terminal : 

```bash
cd frontend
npm run dev
```











## Production deployment

The first step have to be done by a project maintainer, the rest of the documentation can be followed by anyone.
By the way, it's still possible for persons with minimal Docker skills to follow and adapt the procedure to publish images on their own Docker registry.

### [Maintainers team] - build and deliver

The build stage has to be done from the project root and not from a sub directory.

```sh
docker login registry.gitlab.com
```

#### Building & delivering frontend application

```sh
docker build -t registry.gitlab.com/asciiparait/addon-comix-new/frontend:latest -f ./frontend/Dockerfile ./frontend
docker push registry.gitlab.com/asciiparait/addon-comix-new/frontend:latest
```

#### Building & delivering backend application

```sh
docker build -t registry.gitlab.com/asciiparait/addon-comix-new/backend:latest -f ./backend/Dockerfile ./backend
docker push registry.gitlab.com/asciiparait/addon-comix-new/backend:latest
```
