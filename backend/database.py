from pymongo import MongoClient
from bson.objectid import ObjectId


def get_database():
    # FIXME: switch to a singleton pattern
    CONNECTION_STRING = "mongodb://prodComixUser:DatPasswordIsComix@comix-db/admin"
    client = MongoClient(CONNECTION_STRING)
    return client['ComixAddon']



class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")
