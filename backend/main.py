from fastapi import FastAPI, Depends, Request, Body, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from auth_bearer import JWTBearer
from controllers.user import signup, signin, get_profile, update_profile
from controllers.project import save_project, get_projects, get_project
from controllers.file import save_user_profile_image, save_project_image, get_file, get_project_files_list
from controllers.misc import fetch_external_images
from models.Project import Project
import base64
import json
from decouple import config

app = FastAPI()

# Authentication 
JWT_SECRET = config("secret")
JWT_ALGORITHM = config("algorithm")

# CORS integration
origins = [
    "http://localhost:5173",
    "http://localhost:5174",
    "http://localhost:7890",
    "https://comix.asciiparait.fr",
    "*",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# POST Body models 
# TODO: move them to a dedicated file
class SignUpPayload(BaseModel):
    login: str
    password: str
    name: str
    email: str
class SignInPayload(BaseModel):
    login: str
    password: str


@app.get("/")
async def root():
    return {
        "message": "API is alive!",
        "description": "Site API"
    }


#
# Authentication endpoints
#
@app.post("/signup")
async def signup_service(form: SignUpPayload):
    return signup(form.login, form.password, form.name, form.email)
    
@app.post("/signin")
async def signin_service(form: SignInPayload):
    return signin(form.login, form.password)
    
@app.get("/signout")
async def root():
    return {
        "message": "not yet available",
        "description": ""
    }



#
# Artists endpoints
#
@app.get("/artists")
async def root(search: str = ''):
    return {
        "message": "not yet available",
        "description": "list registered artists (allow simple string search as query param)"
    }

@app.get("/artists/{artist_login}")
async def root(artist_login):
    return get_profile(artist_login)

@app.post("/artists/{artist_login}", dependencies=[Depends(JWTBearer())])
async def root(artist_login, request: Request):
    # Check that profile to update matches its author
    # TODO: put it as middleware dependency?!
    payload = request.headers.get('Authorization').split('.')[1]
    payload += '=' * (4 - len(payload) % 4)
    payload = base64.b64decode(payload.encode('ascii'))
    author_login = json.loads(payload)['login']
    if author_login == artist_login:
        body = await request.json()
        update_profile(body)
    # TODO: return result information
    return {
        "message": "not yet available",
        "description": "get a specific artist"
    }

@app.get("/artists/{artist_login}/profile-picture/")
@app.get("/artists/{artist_login}/profile-picture")
async def get_profile_picture(artist_login: str):
    return get_file(f'users/{artist_login}/profile.jpeg')

@app.post("/artists/{artist_login}/profile-picture/")
async def set_profile_picture(artist_login: str, file: UploadFile, request: Request):
    # TODO: put it as middleware dependency?!
    payload = request.headers.get('Authorization').split('.')[1]
    payload += '=' * (4 - len(payload) % 4)
    payload = base64.b64decode(payload.encode('ascii'))
    author_login = json.loads(payload)['login']
    if author_login == artist_login or True:
        file_path = f'/tmp/{file.filename}'
        with open(file_path, 'wb') as f:
            f.write(await file.read())
        return save_user_profile_image(artist_login, file_path)

@app.get("/artists/{artist_id}/projects")
async def root(artist_id, search: str = ''):
    return get_projects(artist_id, search)

@app.get("/projects")
async def root(search: str = ''):
    return get_projects(search=search)

@app.get("/projects/{project_id}")
@app.get("/artists/{artist_id}/projects/{project_id}")
async def root(artist_id: str = '', project_id: str = ''):
    return get_project(project_id)

@app.post("/artists/{artist_login}/projects", dependencies=[Depends(JWTBearer())])
async def root(artist_login, request: Request, project: Project= Body(...)):
    # Check that profile to create a project in matches its author
    # TODO: put it as middleware dependency?!

    payload = request.headers.get('Authorization').split('.')[1]
    payload += '=' * (4 - len(payload) % 4)
    payload = base64.b64decode(payload.encode('ascii'))
    author_login = json.loads(payload)['login']
    if author_login == artist_login:
        return save_project(author_login, project)
    # TODO: return result information
    return {
        "message": "not allowed",
        "description": "get a specific artist"
    }

@app.post("/artists/{artist_login}/projects/{project_id}/resources/")
async def add_or_replace_project_resource(artist_login: str, project_id: str, file: UploadFile, request: Request):
    """ Add or replace the resource with the same name, for a given artist and project"""
    # TODO: put it as middleware dependency?!
    payload = request.headers.get('Authorization').split('.')[1]
    payload += '=' * (4 - len(payload) % 4)
    payload = base64.b64decode(payload.encode('ascii'))
    author_login = json.loads(payload)['login']
    # TODO: add a quota check for the user before inserting
    # FIXME: remove that terrible "True" condition
    if author_login == artist_login or True:
        file_path = f'/tmp/{file.filename}'
        with open(file_path, 'wb') as f:
            f.write(await file.read())
        return save_project_image(artist_login, project_id, file_path)

@app.get("/artists/{artist_login}/projects/{project_id}/resources/")
async def get_project_resources_list(artist_login: str, project_id: str):
    """Get list of all resources of the current project
    
    - if the owner asks for it, returns the full list
    - if it's not the owner, also checks that the project is published
      (returns an empty list, with a 404 error)
    """
    # TODO: code that feature
    return get_project_files_list(f'users/{artist_login}/projects/{project_id}')
    return get_project_files_list(f'users/{artist_login}/projects/')
    return get_project_files_list(f'users/{artist_login}/projects')


@app.get("/artists/{artist_login}/projects/{project_id}/resources/{file_name}")
async def get_project_resource(artist_login: str, project_id: str, file_name: str):
    """Get a given resource for a given project and artist
    
    - if the owner asks for it, returns the file
    - if it's not the owner, also checks that the project is published
      (returns an empty file, with a 404 error)
    """
    # TODO code that feature
    return get_file(f'users/{artist_login}/projects/{project_id}/{file_name}')


@app.get("/extract-image")
async def get_project_resource(url):

    return fetch_external_images(url)


