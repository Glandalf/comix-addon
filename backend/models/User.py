from database import PyObjectId
from pydantic import BaseModel, Field
from bson.objectid import ObjectId



class Profile(BaseModel):
    description: str = ''
    looking_for_projects: bool = False
    working_on_projects: bool = False
    facebook: str = ''
    instagrem: str = ''
    snapchat: str = ''
    tiktok: str = ''
    twitch: str = ''
    website: str = ''

class User(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    email: str = Field(...)
    login: str = Field(...)
    name: str = Field(...)
    profile: Profile | None = None

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "email": "jdoe@example.com",
                "login": "JaneDoe",
                "name": "Jane Doe",
            }
        }

