from database import PyObjectId
from pydantic import BaseModel, Field
from typing import Optional, List
from bson.json_util import dumps, loads


class TeamMember(BaseModel):
    # Alternatively internal_login or every other attributes
    internalLogin: str | None = None
    login: str | None = None
    picture_base64: str | None = None
    tags: list[str]


class InspirationSource(BaseModel):
    title: Optional[str]
    description: str
    links: List[str]


class WorkMethod(BaseModel):
    title: str
    description: str
    tags: list[str]
    base64_illustration: str


class Hero(BaseModel):
    name: str
    description: str
    key: str | int


class Place(BaseModel):
    name: str
    description: str
    key: str | int
    

# class Resource(BaseModel):
#     name: str
#     base64: str


class ProjectSettings(BaseModel):
    background_color: str
    color: str
    font: str



# class NewProject(BaseModel):
#     name: str = ''
#     keywords: list[str] = []
#     presentation: str = ''
#     team: list[TeamMember]= []
#     inspirations: list[InspirationSource] = []
#     workMethods: list[WorkMethod] = []
#     heroes: list[Hero] = []
#     places: list[Place] = []


class Project(BaseModel):
    id: Optional[PyObjectId] = Field(default_factory=PyObjectId, alias="_id")
    isPublished: Optional[bool] = False
    name: str = ''
    author_name: Optional['str']
    keywords: list[str] = []
    presentation: str = ''
    team: list[TeamMember]= []
    inspirations: list[InspirationSource] = []
    workMethods: list[WorkMethod] = []
    heroes: list[Hero] = []
    places: list[Place] = []

    def toJson(self) -> dict:
        return {
            "_id": self.id,
            "name": self.name,
            "isPublished": self.isPublished,
            "author_name": self.author_name,
            "keywords": self.keywords,
            "presentation": self.presentation,
            "team": [dict(item) for item in self.team],
            "inspirations": [dict(item) for item in self.inspirations],
            "workMethods": [dict(item) for item in self.workMethods],
            "heroes": [dict(item) for item in self.heroes],
            "places": [dict(item) for item in self.places],
        }
