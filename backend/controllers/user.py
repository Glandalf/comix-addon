import os, sys
from database import get_database
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
from fastapi import status
from fastapi.responses import Response, JSONResponse
from pprint import pprint
from controllers.project import Project
import bcrypt
import json
import jwt
import time
from decouple import config

db = get_database()


# Auth configuration
JWT_SECRET = config("secret")
JWT_ALGORITHM = config("algorithm")

def generate_token(login: str, name: str, credentials: list=[]) -> str:
    payload = {
        "login": login,
        "name": name,
        "scope": credentials,
        "expires": time.time() + 60000
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    print("GENERATED TOKEN")
    print(token)
    return token

def decode_jwt(token: str) -> dict:
    try:
        decoded_token = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decoded_token if decoded_token["expires"] >= time.time() else None
    except Exception as e:
        pprint("EXCEPTION")
        pprint(e)
        return {}

# 
# User part
# 
def signup(login, password, name, email):
    print('INFO:\t  CONTROLLER - signup user ' + login)
    if db.users.count_documents({'login': login}) <= 0:
        response = db.users.insert_one({
            'login': login,
            'password': bcrypt.hashpw(password.encode(), bcrypt.gensalt()),
            'name': name,
            'email': email,
            })
        user = db.users.find_one({'_id': response.inserted_id})
        user['access_token'] = generate_token(login, name, ['all'])
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=json.loads(dumps(user)))
    else:
        return JSONResponse(status_code=409, content={"error": "001", "message": "This account already exists"})

def signin(login, password):
    print('INFO:\t  CONTROLLER - signin user ' + login)
    user = db.users.find_one({'login': login})
    if user:
        password = password.encode('utf-8')
        if bcrypt.checkpw(password, user['password']):
            print('\t  - Right credentials for user ' + login)
            user['access_token'] = generate_token(user['login'], user['name'], ['all'])
            return JSONResponse(status_code=200, content=json.loads(dumps(user)))
        print('\t  - Wrong password for user ' + login)
    else:
        print('\t  - Unknown user ' + login)
        return JSONResponse(status_code=403, content={"error": "002", "message": "Wrong login or password"})

def removeAccount(login, password):
    pass


# 
# Profiles part
# 


def get_profile(user_login: str):
    print('INFO:\t  CONTROLLER - update profile')
    user = db.users.find_one({'login': user_login})
    # Remove user private data
    del user['name']
    del user['password']
    del user['email']
    return JSONResponse(status_code=200, content=json.loads(dumps(user)))


def update_profile(user) -> bool:
    print('INFO:\t  CONTROLLER - update profile')
    del user['_id']
    result = db.users.update_one({'login': user['login']}, {"$set": user})
    return result.modified_count == 1
