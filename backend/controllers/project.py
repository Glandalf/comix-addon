from fastapi import status
from fastapi.responses import Response, JSONResponse
from models.Project import Project
from database import get_database
from typing import List
from bson.json_util import dumps, loads
from bson.objectid import ObjectId
import json
import re
from pprint import pprint


db = get_database()

def save_project(user_login: str, project: Project):
    print('INFO:\t  CONTROLLER - save project')
    pprint(project)
    # TODO: check that a project with same name and user_login is not already existing
    if not project.id and db.projects.count_documents({"name": project.name, "author_name": project.author_name}) == 1:
        return {}
    if project.id and db.projects.count_documents({"_id": project.id}) == 1:
        return update_project(project)
    else:
        return create_project(user_login, project)

def create_project(user_login: str, project: Project):
    print('INFO:\t  CONTROLLER - create project')
    project.author_name = user_login
    response = db.projects.insert_one(project.toJson())
    project = db.projects.find_one({'_id': response.inserted_id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=json.loads(dumps(project)))

def update_project(project: Project):
    print('INFO:\t  CONTROLLER - update project')
    result = db.projects.update_one({'_id': project.id}, {"$set": project.toJson()})
    project = db.projects.find_one({'_id': project.id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=json.loads(dumps(project)))


# def get_project(id: str) -> Project | bool:
#     pass

def get_projects(artist_login: str | None=None, search: str | None=None) -> Project | bool:
    query_filter = {}
    if artist_login:
        query_filter = {**query_filter, 'author_name': artist_login}
    else:
        query_filter = {**query_filter, 'isPublished': True}
    if search:
        query_filter = {
            **query_filter,
            "$or": [
                { "keywords": search },
                { "name": re.compile(search, re.IGNORECASE) }
            ]
        }
        
    projects = db.projects.find(query_filter)
    return JSONResponse(status_code=200, content=json.loads(dumps(projects)))

def get_project(project_id: str) -> Project | bool:
    # TODO: filter on non published projects
    projects = db.projects.find_one({"_id": ObjectId(project_id)})
    return JSONResponse(status_code=200, content=json.loads(dumps(projects)))

def find_projects(search: str) -> list[Project]:
    pass

def delete_project(project: Project) -> bool:
    pass