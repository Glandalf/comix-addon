import requests
from pprint import pprint
from bs4 import BeautifulSoup


def fetch_external_images(url: str):
    r = requests.get(url)
    mime_type = r.headers['content-type']
    pprint(mime_type)
    if mime_type.startswith('image'):
        pprint('image !')
        return [url]
    elif mime_type.startswith('text/html'):
        soup = BeautifulSoup(r.text, 'html.parser')
        images = soup.find_all('img')
        return [tag['src'] for tag in images]
    return {'error': 'Could not find image from this URL'}
