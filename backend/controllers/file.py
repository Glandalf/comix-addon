from fastapi.responses import Response
from minio import Minio
from minio.error import S3Error
import os
from bson.json_util import dumps, loads


client = Minio(
    os.environ['minioAPIURL'],
    secure=os.environ['minioSecureConnection'] == 'True',
    access_key='write-key',
    secret_key=os.environ['minioWriteKey'],
)


def save_user_profile_image(user_login: str, file_path: str) -> bool:
    print ('--- image upload ---')
    print (user_login)
    print (file_path)
    try:
        client.fput_object("comix", f"users/{user_login}/profile.jpeg", file_path)
        # client.fput_object("comix", f"users/{user_login}/{file_path.split('/')[-1]}", file_path)
    except Exception as e:
        print('Error')
        print(e)

def save_project_image(user_login: str, project_id:str, file_path: str) -> bool:
    try:
        client.fput_object("comix", f"users/{user_login}/projects/{project_id}/{file_path.split('/')[-1]}", file_path)
    except Exception as e:
        print('Error')
        print(e)

def get_file(storage_path: str):
    headers = {'Content-Type': 'image/jpeg'}
    response = client.get_object("comix", storage_path)
    bytes_data = response.data
    response.close()
    response.release_conn()
    return Response(status_code=200, headers=headers, content=bytes_data)

def get_project_files_list(storage_path: str):
    from pprint import pprint
    headers = {'Content-Type': 'application/json'}
    print ('------ ' + storage_path)
    files = []
    response = client.list_objects("comix", prefix=storage_path, recursive=True)
    for obj in response:
        path = obj.object_name.split('/')
        path[0] = '/artists'
        path.insert(4, 'resources')
        files.append('/'.join(path))
    return Response(status_code=200, headers=headers, content=dumps(files))
    # bytes_data = response.data
    # response.close()
    # response.release_conn()
    # return Response(status_code=200, headers=headers, content=bytes_data)
