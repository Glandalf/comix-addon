import { useUserStore } from "@/stores/user"
import type { User } from "@/stores/user"
import type { Project } from "@/models/Project"
import { useProjectstore } from "@/stores/project"
// import { useProjectstore } from "@/stores/project"

export const API_ROOT_URL = 'https://api-comix.asciiparait.fr'

export function signIn(login: string, password: string) {
    // const userStore = useUserStore()
    return fetch(`${API_ROOT_URL}/signin`, {
        method: 'POST',
        headers: {
            authorization: '',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ login, password })
    })
}       

export function signUp(login: string, password: string, name: string, email: string) {
    return fetch(`${API_ROOT_URL}/signup`, {
        method: 'POST',
        headers: {
            authorization: '',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ login, password, name, email })
    })
}

export function getProfile(userLogin: string) {
    const userStore = useUserStore()
    return fetch(`${API_ROOT_URL}/artists/${userLogin}`, {
        method: 'GET',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        },
    })
}

export function getProject(projectId: string) {
    const userStore = useUserStore()
    // const projectStore = useProjectstore()
    return fetch(`${API_ROOT_URL}/projects/${projectId}`, {
        method: 'GET',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        },
    })
}

export function updateProfile(user: User) {
    user = JSON.parse(JSON.stringify(user))
    delete(user.access_token)
    const userStore = useUserStore()
    console.log('User', JSON.parse(JSON.stringify(user)))
    return fetch(`${API_ROOT_URL}/artists/${user.login}`, {
        method: 'POST',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    })
}

export function uploadProfilePicture(user_login: string, file: any) {
    const userStore = useUserStore()
    const formData  = new FormData();
    formData.append('file', file)
    console.log(formData.toString().split('\n'))
    return fetch(`${API_ROOT_URL}/artists/${user_login}/profile-picture/`, {
        method: 'POST',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            // 'Content-Type': undefined,
        },
        body: formData
    })
}

export function uploadProjectResource(user_login: string, project_id: string, file: any, fileName?: string) {
    const userStore = useUserStore()
    const formData  = new FormData();
    console.log('coucou', fileName)
    fileName ||= file.fileName
    console.log('coucou', fileName)
    formData.append('file', file, fileName)
    console.log(formData.toString().split('\n'))
    return fetch(`${API_ROOT_URL}/artists/${user_login}/projects/${project_id}/resources/`, {
        method: 'POST',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            // 'Content-Type': undefined,
        },
        body: formData
    })
}

export function getProjects(artist_login?: string, search?: string) {
    const userStore = useUserStore()
    let artist_path = ''
    let searchParam = ''
    if(artist_login) {
        artist_path = `/artists/${artist_login}`
    }
    if(search) {
        searchParam = `?search=${search}`
    }
    return fetch(`${API_ROOT_URL}${artist_path}/projects${searchParam}`, {
        method: 'GET',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        }
    })
}

export function searchProjects(search?: string) {
    const userStore = useUserStore()
    return fetch(`${API_ROOT_URL}/projects?search=${search}`, {
        method: 'GET',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        }
    })
}

export function getProjectFiles(artist_login: string, project_id: string) {
    // /artists/{artist_login}/projects/{project_id}/resources/
    const userStore = useUserStore()
    return fetch(`${API_ROOT_URL}/artists/${artist_login}/projects/${project_id}/resources/`, {
        method: 'GET',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        }
    })
}

export function updateProject(project: Project) {
    project = JSON.parse(JSON.stringify(project))
    // Fix nested oid key in _id for Fast API model checking
    if (project._id && project._id['$oid']) {
        (project._id as any) = project._id['$oid']
    }
    const userStore = useUserStore()
    console.log('Project', JSON.parse(JSON.stringify(project)))
    return fetch(`${API_ROOT_URL}/artists/${userStore.user.login}/projects`, {
        method: 'POST',
        headers: {
            authorization: `Bearer ${userStore.user.access_token}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(project)
    })
}
