import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createI18n } from 'vue-i18n'
// TODO : dynamic import right file(s)
import fr from './i18n/fr.json'
import es from './i18n/es.json'
import it from './i18n/it.json'
import tr from './i18n/tr.json'

type MessageSchema = typeof fr

const i18n = createI18n<[MessageSchema], 'fr-FR'>({
    legacy: false,
    // TODO: detect user default language
    locale: 'fr-FR',
    messages: {
        'es-ES': es,
        'fr-FR': fr,
        'it-IT': it,
        'tr-TR': tr,
    } as any
})



import App from './App.vue'
import router from './router'
import './assets/main.css'

const app = createApp(App)

app.use(i18n)
app.use(createPinia())
app.use(router)

app.mount('#app')
