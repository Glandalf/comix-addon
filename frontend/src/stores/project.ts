import { ref, } from 'vue'
import { defineStore } from 'pinia'
import type { Project } from '@/models/Project'

export const useProjectstore = defineStore('project', () => {
    const project = ref<Project>({
        name: '',
        keywords: [],
        presentation: '',
        team: [],
        inspirations: [],
        workMethods: [],
        heroes: [],
        places: [],
    })
    const projectsList = ref<Array<Project>>([])

    return { project, projectsList }
})
