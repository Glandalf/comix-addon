import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useNotifications =  defineStore('notifications', () => {
    const message = ref('Coucou')
    const type = ref('')
    const isVisible = ref(false)
    const duration = ref(3000)

    function showNotification(notificationMessage: string, notificationType?: string,  notificationDuration?: number) {
        message.value = notificationMessage
        notificationType? type.value = notificationType : null
        notificationDuration? duration.value = notificationDuration : null
        
        isVisible.value = true
        setTimeout(() => { isVisible.value = false }, duration.value)
    }

    return { showNotification, message, type, isVisible, duration }
})
