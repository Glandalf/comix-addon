import { ref, } from 'vue'
import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const user = ref<User>({
    _id: { $oid: '' },
    login: '',
    email: '',
    name: '',
    lang: '',
    profilePicture: false,
  })

  function connect(loggedUser: User) {
    updateLocally(loggedUser)
  }

  function updateLocally(loggedUser: User) {
    /**
     * Update Local storage and store with provided data
     */
    user.value = loggedUser
    window.sessionStorage.setItem('logged_user', JSON.stringify(loggedUser))

  }

  function disconnect() {
    window.sessionStorage.removeItem('logged_user')
    user.value = {
      _id: { $oid: '' },
      login: '',
      email: '',
      name: '',
      lang: '',
      profilePicture: false,
    }
  }

  function initFromSessionStorage() {
    /**
     * Used to initiate User store from SessionStorage on App start
     */
    const storedUser = window.sessionStorage.getItem('logged_user')
    if (storedUser) {
      user.value = JSON.parse(storedUser)
    }
  }

  return { user, connect, updateLocally, initFromSessionStorage, disconnect }
})



export type User = {
  _id?: { $oid: string }
  login: string
  email: string
  name: string
  lang: string
  access_token?: any
  profile?: Profile
  profilePicture: false
}

export type Profile = {
  presentation?: string
  website: string
  facebook: string
  instagram: string
  snapchat: string
  tiktok: string
  twitch: string
  photo: string
  preferedStyles: Array<string>
  preferedTechnologies: Array<string>
}
