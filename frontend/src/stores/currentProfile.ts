import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Profile } from '@/stores/user'

export const useCurrentProfileStore = defineStore('currentProfile', () => {
  const login = ref('')
  const profile = ref<Profile>({
    website: '',
    facebook: '',
    instagram: '',
    snapchat: '',
    tiktok: '',
    twitch: '',
    photo: '',
    preferedStyles: [],
    preferedTechnologies: [],
  })

  
  function save(profileLogin: string, newProfile: Profile) {
    login.value = profileLogin
    console.debug('PROFILE BEFORE STORE', profile)
    profile.value.website = newProfile.website
    profile.value.facebook = newProfile.facebook
    profile.value.instagram = newProfile.instagram
    profile.value.snapchat = newProfile.snapchat
    profile.value.tiktok = newProfile.tiktok
    profile.value.twitch = newProfile.twitch
    profile.value.photo = newProfile.photo
    profile.value.preferedStyles = newProfile.preferedStyles
    profile.value.preferedTechnologies = newProfile.preferedTechnologies
  }

  return { login, profile, save, }
})


