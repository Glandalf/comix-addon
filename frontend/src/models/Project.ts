// TODO: fix "any"s
export type Project = {
    _id?: { $oid: string }
    author_name?: string
    name: string
    keywords: Array<string>
    presentation: string
    team: Array<any>
    inspirations: Array<Inspiration>
    workMethods: Array<any>
    heroes: Array<any>
    places: Array<any>
    calculatedFiles?: Array<string>
    isPublished?: boolean
  }
  
  
  export type Inspiration = {
    description : string,
    links: Array<string>,
    linkToAdd?: string
  }