export type TeamMember = {
    internalLogin?: string
    login?: string
    picture?: string
    tags?: Array<string> 
}