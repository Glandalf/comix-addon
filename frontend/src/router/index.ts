import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/AuthenticationView.vue'
import { useUserStore } from '@/stores/user'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'login',
      component: HomeView
    },
    {
      path: '/profile/:login',
      name: 'profile',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/ProfileView.vue')
    },
    {
      path: '/create-project',
      name: 'create-project',
      component: () => import('../views/ProjectCreationView.vue')
    },
    {
      path: '/edit-project/:projectId',
      name: 'edit-project',
      component: () => import('../views/ProjectCreationView.vue')
    },
    {
      path: '/projects/:projectId',
      name: 'view-project',
      component: () => import('../views/ProjectView.vue')
    },
    {
      path: '/edit-project-details/:projectId',
      name: 'detail-project-details',
      component: () => import('../views/ProjectDetailsEditionView.vue')
    },
    {
      path: '/share-project/:projectId',
      name: 'share-project',
      component: () => import('../views/ProjectSocialShare.vue')
    },
    {
      path: '/my-projects',
      name: 'my-project',
      component: () => import('../views/ProjectsView.vue')
    },
    {
      path: '/projects',
      name: 'project',
      component: () => import('../views/ProjectsView.vue')
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  const userStore = useUserStore()
  if (!userStore.user?.access_token && to.name !== 'login') {
    next({ name: 'login' })
  }
  else {
    next()
  }
})

export default router
